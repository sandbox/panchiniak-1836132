README.txt
==========
Objective
=========
Sky Texture Changer is a simple module for changing the texture of
sky theme.
AUTHOR
======
Rodrigo Panchiniak Fernandes <fernandesrp at cce DOT ufsc DOT br>
http://www.cce.ufsc.br/~fernandesrp
